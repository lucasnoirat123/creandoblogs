from django.db import models
from ckeditor.fields import RichTextField
# Create your models here.
class Categoria(models.Model):
    id = models.AutoField(primary_key=True)
    nombre= models.CharField('categoria', max_length=100, null=False, blank=False)
    estado = models.BooleanField('activado', default=True)
    fecha_creacion = models.DateField('fecha de cracion', auto_now=False, auto_now_add=True)

    class Meta:
        verbose_name = 'categoria'
        verbose_name_plural = 'categorias'

    def __str__(self):
        return self.nombre




class Autor(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField('nombre autor', max_length=30, null=False, blank=False)
    apellido = models.CharField('apellido autor', max_length=30, null=False, blank=False)
    facebook = models.URLField('facebook', null=True ,blank= True)
    twitter = models.URLField('twitter', null=True ,blank= True)
    instagram = models.URLField('instagram', null=True ,blank= True)
    email = models.EmailField('email', null=False, blank=False)
    estado = models.BooleanField('actividad', default=True)
    fecha_cracion = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name = 'autor'
        verbose_name_plural = 'autores'
    
    def __str__(self):
        return "{0},{1}".format(self.apellido, self.nombre)




class Post(models.Model):
    id = models.AutoField(primary_key=True)
    titulo = models.CharField('titulo',max_length=30, blank=False, null=False)
    slug = models.CharField('slug', max_length=100, null=False, blank=False)
    contenido = RichTextField()
    descripcion = models.CharField(max_length=100, null=False, blank=False)
    imagen    =models.URLField(max_length=100, blank=False,null=False)
    autor   = models.ForeignKey(Autor, on_delete=models.CASCADE)
    estado =models.BooleanField('publicado/SI/NO', default=True)
    fecha_cracion = models.DateField(auto_now=False, auto_now_add=True)
   
    class Meta:
        verbose_name= 'Post'
        verbose_name_plural = 'Posts'



    def __str__(self):
        return self.titulo      
