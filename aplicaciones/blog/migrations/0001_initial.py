# Generated by Django 3.2.3 on 2021-05-26 16:38

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Autor',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=30, verbose_name='nombre autor')),
                ('apellido', models.CharField(max_length=30, verbose_name='apellido autor')),
                ('facebook', models.URLField(blank=True, null=True, verbose_name='facebook')),
                ('twitter', models.URLField(blank=True, null=True, verbose_name='twitter')),
                ('instagram', models.URLField(blank=True, null=True, verbose_name='instagram')),
                ('email', models.EmailField(max_length=254, verbose_name='email')),
                ('estado', models.BooleanField(default=True, verbose_name='actividad')),
                ('fecha_cracion', models.DateField(auto_now=True)),
            ],
            options={
                'verbose_name': 'autor',
                'verbose_name_plural': 'autores',
            },
        ),
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=100, verbose_name='categoria')),
                ('estado', models.BooleanField(default=True, verbose_name='activado')),
                ('fecha_creacion', models.DateField(auto_now_add=True, verbose_name='fecha de cracion')),
            ],
            options={
                'verbose_name': 'categoria',
                'verbose_name_plural': 'categorias',
            },
        ),
    ]
