from django.contrib import admin
from .models import Autor , Categoria, Post
from import_export import resources
from import_export.admin import ImportExportModelAdmin

# Register your models here.


class CategoriaResources(resources.ModelResource):
    class Meta:
        model = Categoria

class CategoriaAdmin( ImportExportModelAdmin,admin.ModelAdmin):
    search_fields= ['nombre']
    list_display = ('nombre', 'estado', 'fecha_creacion')
    resource_class = CategoriaResources



class AutorResources(resources.ModelResource):
    class Meta:
        model = Autor

class AutorAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    search_fields = ['nombre', 'apellido', 'email']
    list_display = ('nombre','apellido','email')
    resource_class = AutorResources
    

admin.site.register(Autor,AutorAdmin)
admin.site.register(Categoria,CategoriaAdmin)   
admin.site.register(Post)
